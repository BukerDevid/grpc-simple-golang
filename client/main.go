package main

import (
	"context"
	"fmt"
	"io"
	"time"

	"client/tracetest"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

func main() {
	options := []grpc.DialOption{}
	options = append(options, grpc.WithInsecure())
	conn, err := grpc.Dial(":10981", options...)
	if err != nil {
		logrus.Panic(err)
	}
	client := tracetest.NewTraceTestClient(conn)
	count := 0

	for {
		res, err := Echo(context.Background(), client, fmt.Sprintf("test-%d", count))
		if err != nil {
			logrus.Error(err)
			return
		}
		if res.Text != "" {
			logrus.Info("Read response OK!")
		}

		if count > 5 {
			break
		}
		count++
	}

	time.Sleep(time.Second * 1)
	if err := CallStreamRead(client); err != nil {
		logrus.Error(err)
	}

	time.Sleep(time.Second * 1)
	if err := CallStreamWrite(client); err != nil {
		logrus.Error(err)
	}

	time.Sleep(time.Second * 1)
	if err := CallStream(client); err != nil {
		logrus.Error(err)
	}
}

func Echo(ctx context.Context, conn tracetest.TraceTestClient, msg string) (*tracetest.Rp_Test, error) {
	logrus.Info("Write message - ", msg)
	request := &tracetest.Rq_Test{
		Text: msg,
	}
	return conn.CheckTest(ctx, request)
}

func CallStreamRead(conn tracetest.TraceTestClient) error {
	logrus.Info("[CALL STREAM READ]")
	defer logrus.Info("[CALL STREAM READ] end")

	stream, err := conn.CheckStreamRead(context.Background(), &tracetest.Rq_Test{Text: "Init read"})
	if err != nil {
		return err
	}

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			logrus.Info("EOF reader")
			break
		}
		if err != nil {
			return err
		}

		logrus.Info("Read server msg - ", msg.Text)
	}

	return nil
}

func CallStreamWrite(conn tracetest.TraceTestClient) error {
	logrus.Info("[CALL STREAM WRITE] - start")
	defer logrus.Info("[CALL STREAM WRITE] - end")
	count := 0

	stream, err := conn.CheckStreamWrite(context.Background())
	if err != nil {
		return err
	}

	for {
		if count == 5 {
			break
		}

		if err := stream.Send(&tracetest.Rq_Test{Text: fmt.Sprintf("[READ STREAM] - send msg %d", count)}); err != nil {
			logrus.Error(err)
		}

		count++
		time.Sleep(time.Microsecond * 100)
	}

	return nil
}

func CallStream(conn tracetest.TraceTestClient) error {
	logrus.Info("[CALL STREAM] - start")
	defer logrus.Info("[CALL STREAM] - end ")

	outChan := make(chan struct{})
	count := 0

	stream, err := conn.CheckStream(context.Background())
	if err != nil {
		return err
	}

	go func() {
		for {
			msg, err := stream.Recv()
			if err == io.EOF {
				logrus.Info("EOF reader")
				return
			}
			if err != nil {
				logrus.Error(err)
				return
			}

			logrus.Info("Read server msg - ", msg.Text)
		}

	}()

	for {
		if count == 5 {
			break
		}

		if err := stream.Send(&tracetest.Rq_Test{Text: fmt.Sprintf("[STREAM] - send msg %d", count)}); err != nil {
			logrus.Fatal(err)
			break
		}

		time.Sleep(time.Microsecond * 100)
		count++
	}

	stream.CloseSend()
	<-outChan
	return nil
}
