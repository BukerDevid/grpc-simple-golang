package main

import (
	"context"
	"fmt"
	"io"
	"net"
	"os"
	"runtime"
	"runtime/pprof"
	"server/tracetest"
	pb "server/tracetest"
	"sync"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

func initGRPCServerForTest(quit <-chan struct{}) {
	listen, err := net.Listen("tcp4", ":8098")
	if err != nil {
		logrus.Panic(err)
	}

	var opts []grpc.ServerOption
	opts = append(opts, grpc.KeepaliveParams(keepalive.ServerParameters{
		MaxConnectionIdle: 2 * time.Hour,
	}))

	InitGRPCServer()
	grpcServer := grpc.NewServer(opts...)

	logrus.Info("[Start GRPC]\t RUN!")
	pb.RegisterTraceTestServer(grpcServer, GRPCServer)

	go func() {
		go grpcServer.Serve(listen)
		<-quit
		logrus.Info("Close gRPC server")
	}()
}

func initGRPCClientForTest(waitGroup *sync.WaitGroup) {
	defer waitGroup.Done()

	options := []grpc.DialOption{}
	options = append(options, grpc.WithInsecure())

	conn, err := grpc.Dial(":10981", options...)
	if err != nil {
		logrus.Panic(err)
	}

	client := tracetest.NewTraceTestClient(conn)
	count := 0

	for {
		res, err := echo(context.Background(), client, fmt.Sprintf("test-%d", count))
		if err != nil {
			logrus.Error(err)
			return
		}

		if res.Text != "" {
			logrus.Info("Read response OK!")
		}

		if count > 5 {
			break
		}
		count++
	}

	if err := callStreamRead(client); err != nil {
		logrus.Error(err)
	}

	if err := callStreamWrite(client); err != nil {
		logrus.Error(err)
	}

	if err := callStream(client); err != nil {
		logrus.Error(err)
	}
}

func initGRPCClientForTestType() {
	options := []grpc.DialOption{}
	options = append(options, grpc.WithInsecure())

	conn, err := grpc.Dial(":10981", options...)
	if err != nil {
		logrus.Panic(err)
	}

	client := tracetest.NewTraceTestClient(conn)

	checkType, err := check_type(context.Background(), client, "call for test")
	if err != nil {
		logrus.Panic(err)
	}

	fmt.Println(checkType.CheckArray)
	fmt.Println(checkType.CheckMap)
	fmt.Println(checkType.PointArray)
	fmt.Println(checkType.PointMap)
	fmt.Println(checkType.CheckTimestamp.AsTime())
}

func check_type(ctx context.Context, conn tracetest.TraceTestClient, msg string) (*tracetest.CheckTypes, error) {
	logrus.Info("Write message - ", msg)
	request := &tracetest.Rq_Test{
		Text: msg,
	}
	return conn.CheckType(ctx, request)
}

func echo(ctx context.Context, conn tracetest.TraceTestClient, msg string) (*tracetest.Rp_Test, error) {
	logrus.Info("Write message - ", msg)
	request := &tracetest.Rq_Test{
		Text: msg,
	}
	return conn.CheckTest(ctx, request)
}

func callStreamRead(conn tracetest.TraceTestClient) error {
	logrus.Info("[CALL STREAM READ]")
	defer logrus.Info("[CALL STREAM READ] end")

	stream, err := conn.CheckStreamRead(context.Background(), &tracetest.Rq_Test{Text: "Init read"})
	if err != nil {
		return err
	}

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			logrus.Info("EOF reader")
			break
		}
		if err != nil {
			return err
		}

		logrus.Info("Read server msg - ", msg.Text)
	}

	return nil
}

func callStreamWrite(conn tracetest.TraceTestClient) error {
	logrus.Info("[CALL STREAM WRITE] - start")
	defer logrus.Info("[CALL STREAM WRITE] - end")
	count := 0

	stream, err := conn.CheckStreamWrite(context.Background())
	if err != nil {
		return err
	}

	for {
		if count == 5 {
			break
		}

		if err := stream.Send(&tracetest.Rq_Test{Text: fmt.Sprintf("[READ STREAM] - send msg %d", count)}); err != nil {
			logrus.Error(err)
		}

		count++
		time.Sleep(time.Microsecond * 100)
	}

	return nil
}

func callStream(conn tracetest.TraceTestClient) error {
	logrus.Info("[CALL STREAM] - start")
	defer logrus.Info("[CALL STREAM] - end ")

	outChan := make(chan struct{})
	count := 0

	stream, err := conn.CheckStream(context.Background())
	if err != nil {
		return err
	}

	go func() {
		for {
			msg, err := stream.Recv()
			if err == io.EOF {
				logrus.Info("EOF reader")
				close(outChan)
				break
			}
			if err != nil {
				logrus.Error(err)
				return
			}

			logrus.Info("Read server msg - ", msg.Text)
		}

	}()

	for {
		if count == 5 {
			break
		}

		if err := stream.Send(&tracetest.Rq_Test{Text: fmt.Sprintf("[STREAM] - send msg %d", count)}); err != nil {
			logrus.Fatal(err)
			break
		}

		time.Sleep(time.Microsecond * 100)
		count++
	}

	stream.CloseSend()
	<-outChan
	return nil
}

func TestCpuGRPCServerWithClient(t *testing.T) {
	var waitGroup sync.WaitGroup
	logrus.Info("INIT ALL")
	cpu, err := os.Create("cpuProfile.out")
	if err != nil {
		logrus.Panic(err)
	}
	defer cpu.Close()

	pprof.StartCPUProfile(cpu)
	defer pprof.StopCPUProfile()
	quit := make(chan struct{})

	logrus.Info("START TEST")
	initGRPCServerForTest(quit)
	for idx := 0; idx < 1000; idx++ {
		waitGroup.Add(1)
		go initGRPCClientForTest(&waitGroup)
	}

	logrus.Info("END TEST")

	waitGroup.Wait()
	close(quit)
	runtime.GC()
}

func TestMemGRPCServerWithClient(t *testing.T) {
	var waitGroup sync.WaitGroup

	logrus.Info("INIT ALL")
	memory, err := os.Create("memoryProfile.out")
	if err != nil {
		logrus.Panic(err)
	}
	defer memory.Close()

	quit := make(chan struct{})

	logrus.Info("START TEST")
	initGRPCServerForTest(quit)
	for idx := 0; idx < 1000; idx++ {
		waitGroup.Add(1)
		go initGRPCClientForTest(&waitGroup)
	}

	logrus.Info("END TEST")
	err = pprof.WriteHeapProfile(memory)
	if err != nil {
		logrus.Panic(err)
	}

	waitGroup.Wait()
	close(quit)
	runtime.GC()
}

func TestOutType(t *testing.T) {
	logrus.Info("INIT ALL")
	memory, err := os.Create("memoryProfile.out")
	if err != nil {
		logrus.Panic(err)
	}
	defer memory.Close()

	quit := make(chan struct{})

	logrus.Info("START TEST")

	initGRPCServerForTest(quit)
	initGRPCClientForTestType()

	logrus.Info("END TEST")
	err = pprof.WriteHeapProfile(memory)
	if err != nil {
		logrus.Panic(err)
	}

	close(quit)
	runtime.GC()
}
