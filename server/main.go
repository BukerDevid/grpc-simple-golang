package main

import (
	"context"
	"io"
	"net"
	"time"

	pb "server/tracetest"

	loggo "github.com/bukerdevid/log-go-log"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/protobuf/types/known/timestamppb"
)

const (
	MSG_SRV_CALL_ECHO               = "[SERVER ECHO]"
	MSG_SRV_CALL_CHECKTYPE          = "[SERVER CHECKTYPE]"
	MSG_SRV_CALL_READ_STREAM_Start  = "[SERVER CREATE READ STREAM]"
	MSG_SRV_CALL_READ_STREAM_End    = "[SERVER DEAD READ STREAM]"
	MSG_SRV_CALL_WRITE_STREAM_Start = "[SERVER CREATE WRITE STREAM]"
	MSG_SRV_CALL_WRITE_STREAM_End   = "[SERVER DEAD WRITE STREAM]"
	MSG_SRV_CALL_STREAM_Start       = "[SERVER CREATE DUPLEX STREAM]"
	MSG_SRV_CALL_STREAM_End         = "[SERVER CREATE DUPLEX STREAM]"
)

func InitGRPCServer() {
	GRPCServer = &gRPCServer{}
}

var GRPCServer *gRPCServer

func main() {
	logrus.Info("Server start ...")
	loggo.InitCastomLogger(&logrus.JSONFormatter{TimestampFormat: "15:04:05 02/01/2006"}, logrus.TraceLevel, false, true)

	listen, err := net.Listen("tcp4", ":10981")
	if err != nil {
		logrus.Panic(err)
	}

	var opts []grpc.ServerOption
	opts = append(opts, grpc.KeepaliveParams(keepalive.ServerParameters{
		MaxConnectionIdle: 2 * time.Hour,
	}))

	InitGRPCServer()
	grpcServer := grpc.NewServer(opts...)

	logrus.Info("[Start GRPC]\t RUN!")
	pb.RegisterTraceTestServer(grpcServer, GRPCServer)

	if err := grpcServer.Serve(listen); err != nil {
		logrus.Error(err)
	}
	logrus.Info("Close gRPC server")
}

type gRPCServer struct{}

func (srv *gRPCServer) CheckTest(ctx context.Context, in *pb.Rq_Test) (*pb.Rp_Test, error) {
	logrus.Info(MSG_SRV_CALL_ECHO, in.Text)
	return &pb.Rp_Test{Text: in.Text}, nil
}

func (srv *gRPCServer) CheckStreamRead(in *pb.Rq_Test, stream pb.TraceTest_CheckStreamReadServer) error {
	logrus.Info(MSG_SRV_CALL_READ_STREAM_Start)
	defer logrus.Info(MSG_SRV_CALL_READ_STREAM_End)
	count := 0

	for {
		if err := stream.Send(&pb.Rp_Test{Text: "check"}); err != nil {
			logrus.Error(err)
			return err
		}

		if count == 5 {
			logrus.Debug("End cicle")
			return nil
		}

		count++
		time.Sleep(time.Microsecond * 100)
	}
}

func (srv *gRPCServer) CheckStreamWrite(stream pb.TraceTest_CheckStreamWriteServer) error {
	logrus.Info(MSG_SRV_CALL_WRITE_STREAM_Start)
	defer logrus.Info(MSG_SRV_CALL_WRITE_STREAM_End)

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			return stream.SendAndClose(&pb.Rp_Test{Text: "close"})
		}
		if err != nil {
			return err
		}

		logrus.Info("[WRITE STREAM] - read client msg", msg)
	}

}

func (srv *gRPCServer) CheckStream(stream pb.TraceTest_CheckStreamServer) error {
	logrus.Info(MSG_SRV_CALL_STREAM_Start)
	defer logrus.Info(MSG_SRV_CALL_STREAM_End)

	for {
		msg, err := stream.Recv()
		if err == io.EOF {
			logrus.Info("EOF - exit")
			break
		}
		if err != nil {
			return err
		}

		if msg == nil {
			continue
		}

		logrus.Info("[STREAM] - read client msg", msg.Text)
		stream.Send(&pb.Rp_Test{Text: msg.Text})
	}

	return nil
}

func (srv *gRPCServer) CheckType(context.Context, *pb.Rq_Test) (*pb.CheckTypes, error) {
	logrus.Info(MSG_SRV_CALL_CHECKTYPE)
	return &TypeForCheck, nil
}

var TypeForCheck = pb.CheckTypes{
	CheckArray: []int32{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
	CheckMap: map[int32]string{
		1: "test1",
		2: "test2",
		3: "test3",
		4: "test4",
		5: "test5",
	},
	PointArray: []*pb.Point{
		{
			Lan: 1,
			Lon: 2,
		},
		{
			Lan: 3,
			Lon: 4,
		},
		{
			Lan: 5,
			Lon: 6,
		},
		{
			Lan: 7,
			Lon: 8,
		},
		{
			Lan: 9,
			Lon: 0,
		},
		{
			Lan: 1,
			Lon: 2,
		},
		{
			Lan: 3,
			Lon: 4,
		},
	},
	PointMap: map[int32]*pb.Point{
		1: &pb.Point{
			Lan: 1,
			Lon: 2,
		},
		2: &pb.Point{
			Lan: 1,
			Lon: 2,
		},
		3: &pb.Point{
			Lan: 1,
			Lon: 2,
		},
		4: &pb.Point{
			Lan: 1,
			Lon: 2,
		},
	},
	CheckTimestamp: timestamppb.New(time.Now()),
}
