module server

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/sirupsen/logrus v1.8.1
	google.golang.org/grpc v1.43.0
)

require (
	github.com/bukerdevid/log-go-log v0.0.6
	golang.org/x/net v0.0.0-20201202161906-c7110b5ffcbb // indirect
	google.golang.org/protobuf v1.26.0
)
